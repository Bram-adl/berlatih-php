<?php
function ubah_huruf($string){
  $alphabet = [
    'a', 'b', 'c', 'd', 'e',
    'f', 'g', 'h', 'i', 'j',
    'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't',
    'u', 'v', 'w', 'x', 'y',
    'z'
  ];
  
  // suppose $string equals to bram
  $changed_word = '';

  for ( $i = 0; $i < strlen($string); $i++ ) {
    /*
      we get the first word and match it to our array
      so that we get the current index of the word that
      being iterated and keep it into $current_index.

      say we get first word is b so the index would be 1.

      next we add that with 1 to get the next index which
      should be the next alphabet comes afterwards, so we
      get c.

      then we access the value from the array using the
      $next_index value which is 2 and assign it to the
      $changed_word variable.
    */

    $current_index = array_search(strtolower($string[$i]), $alphabet);
    $next_index = $current_index + 1;

    $changed_word .= $alphabet[$next_index];
  }
  
  return $changed_word . "<br>";
}


// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>